from dataclasses import dataclass


@dataclass
class Person:
    name: str
    lastname: str
    age: int


class Car:
    def __init__(self, model, company):
        self.model = model
        self._company = company

    @property
    def company(self):
        return self._company

    @company.setter
    def company(self, value):
        self._company = value
