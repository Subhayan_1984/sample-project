from sample.main import Person, Car


def test_person():
    p = Person(name="Subhayan", lastname="Bhattacharya", age=34)


def test_car():
    c = Car("Eon", "Hyundai")


def test_car_can_get_company():
    c = Car("Eon", "Hyundai")
    assert c.company == "Hyundai"


def test_car_can_change_model():
    c = Car("Eon", "Hyundai")
    assert c.model == "Eon"
    c.model = "Sonata"
    assert c.model == "Sonata"


def test_car_can_change_model():
    c = Car("Eon", "Hyundai")
    assert c.company == "Hyundai"
    c.model = "Skoda"
    assert c.model == "Skoda"
