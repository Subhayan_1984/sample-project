# the inclusion of the tests module is not meant to offer best practices for
# testing in general, but rather to support the `find_packages` example in
# setup.py that excludes installing the "tests" package


def test_success():
    assert True


def test_version():
    import sample
    print(sample.__version__)


def test_import_sample():
    import sample


def test_fail():
    assert False
